import {defineAsyncComponent} from "vue"

export default async ({ app }) => {
  // Roles
  app.component('zoomable-roles', defineAsyncComponent(() => import('../components/roles/ZoomableRoles.vue')))
  app.component('role-property', defineAsyncComponent(() => import('../components/roles/RoleProperty.vue')))
  app.component('role-select',  defineAsyncComponent(() => import('../components/roles/RoleSelect.vue')))
  app.component('list-roles',  defineAsyncComponent(() => import('../components/roles/ListRoles.vue')))
  app.component('guild-view',  defineAsyncComponent(() => import('../components/roles/GuildView.vue')))
  app.component('role-expansion-item',  defineAsyncComponent(() => import('../components/roles/RoleExpansionItem.vue')))
  app.component('role-print',  defineAsyncComponent(() => import('../components/roles/RolePrint.vue')))
  app.component('roles-print',  defineAsyncComponent(() => import('../components/roles/RolesPrint.vue')))
  // Actors
  app.component('actor-select',  defineAsyncComponent(() => import('../components/actors/ActorSelect.vue')))
  app.component('actor-avatar',  defineAsyncComponent(() => import('../components/actors/ActorAvatar.vue')))
  app.component('actors-avatars',  defineAsyncComponent(() => import('../components/actors/ActorsAvatars.vue')))
  app.component('actors-toggle',  defineAsyncComponent(() => import('../components/actors/ActorsToggle.vue')))
  // Others
  app.component('role-actions-list',  defineAsyncComponent(() => import('../components/RoleActionsList.vue')))
  app.component('dialog-action',  defineAsyncComponent(() => import('../components/DialogAction.vue')))
  app.component('action-item',  defineAsyncComponent(() => import('../components/ActionItem.vue')))
  app.component('notes-list',  defineAsyncComponent(() => import('../components/notes/NotesList.vue')))
  app.component('note-select',  defineAsyncComponent(() => import('../components/notes/NoteSelect.vue')))
}
