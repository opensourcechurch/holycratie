// import { firestorePlugin } from 'vuefire'
import { SB } from "../db"
import _ from 'lodash'
import md5 from "md5"
// var VueD3 = require('vue-d3')


// Time Ago
import TimeAgo from 'javascript-time-ago'
import fr from 'javascript-time-ago/locale/fr'
TimeAgo.addDefaultLocale(fr)
const timeAgo = new TimeAgo('fr-CH')
import { date } from 'quasar'

const roleIcon = {
  owner: "star",
  edit: "edit",
  read: "visibility"
}

import { useOrgaStore } from 'stores/orga';

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
export default async ({ app, Vue, store }) => {
  // app.use(VueD3)
  // Object.defineProperty(Vue.prototype, '$db', { value: db })
  // Object.defineProperty(Vue.prototype, '$fb', { value: firebase })
  // Object.defineProperty(Vue.prototype, '$fs', { value: firebase.firestore })
  // Object.defineProperty(Vue.prototype, '_', { value: _ })

  // app.config.globalProperties.$db = db
  app.config.globalProperties._ = _
  const orgastore = useOrgaStore();
  app.config.globalProperties.$hc = {
    orga: {
      // ref: () => db.collection("orga").doc(store.state.orga.id),
      // // Orga
      // orga: () => store.state.orga.orga || {},
      // newOrga: (payload) => store.dispatch("orga/newOrga", payload),
      // // Actors
      // actors: () => store.state.orga.actors,
      // actorById: id => store.getters["orga/actorById"](id),
      // actorsByRole: id => store.getters["orga/actorsByRole"](id),
      // allActorsByRole: id => store.getters["orga/allActorsByRole"](id),
      // // Roles
      // roles: () => store.state.orga.roles,
      // roleById: id => store.getters["orga/roleById"](id),
      // rolesByParent: id => store.getters["orga/rolesByParent"](id),
      // rolesHierarchy: () => store.getters["orga/rolesHierarchy"],
      // isCircle: (id) => store.getters["orga/isCircle"](id),
      // rolesOrphans: () => store.getters["orga/rolesOrphans"],
      // rolesByActor: id => store.getters["orga/rolesByActor"](id),
      // roleBreadcrumbs: id => store.getters["orga/roleBreadcrumbs"](id),
      // toggleActorInRole: (aId, rId) => store.dispatch("orga/toggleActorInRole", { actor: aId, role: rId }),
      // // Terminology
      // // Templates
      // colors: () => store.state.orga.orga.colors || {},
      // templates: () => store.state.orga.templates,
      // template: (id) => store.getters["orga/template"](id),
      // // Actions
      // actionsByActor: (id) => store.getters["orga/actionsByActor"](id),
      // checkAction: (id, val) => store.dispatch("orga/checkAction", { id: id, value: val }),
      // actionsRef: () => db.collection("orga").doc(store.state.orga.id).collection("actions"),
      // // Documents
      // documentById: (id) => store.getters["orga/documentById"](id),
      // documentsByRole: (id) => store.getters["orga/documentsByRole"](id),
      // documentsByRoleAndChildren: (id) => store.getters["orga/documentsByRoleAndChildren"](id),
      // archiveDocument: (id, val) => store.dispatch("orga/archiveDocument", { id: id, value: val }),
      // documentsRef: () => db.collection("orga").doc(store.state.orga.id).collection("documents"),
      // // Stuff
      // bind: (id) => store.dispatch("orga/bind", id),
      // userRole: () => store.getters["auth/logged"] && store.state.orga.orga.access && store.state.orga.orga.access[store.getters["auth/uid"]]
    },
    avatar: email => "https://www.gravatar.com/avatar/" + md5(email || "") + "?d=retro",
    // userRef: () => db.collection("users").doc(store.getters["auth/uid"]),
    // roleIcon: (role) => roleIcon[role],
    // ts: () => firebase.firestore.FieldValue.serverTimestamp(),
    // // Filters
    // // Remplace les retours à la ligne
    // formatted: txt => txt = marked(txt || ""), → orgaStore.formatted
    // // Transforme en temps relatif (il y a 2mn ...)
    // ago: obj => obj ? timeAgo.format(obj.toDate()) : obj,
    // // Formate un timestamp en qqch de plus lisible
    // date: obj => obj ? date.formatDate(obj.toDate(), 'Le D MMMM YYYY à HH:mm') : obj
  }
}
