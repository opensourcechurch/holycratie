
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: "Home", component: () => import('pages/IndexPage.vue'), props: true },
      { path: 'login', name: "Login", component: () => import('pages/LoginPage.vue') }
    ]
  },
  {
    path: '/user',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: "User", component: () => import('pages/UserPage.vue'), meta: { requiresAuth: true } }
    ]
  },
  {
    path: '/orga/:orga',
    component: () => import('src/layouts/OrgaLayout.vue'),
    props: true,
    children: [
      { path: 'actors/:actor_id?', name: "Actors", component: () => import('pages/orga/ActorsPage.vue'), props: true },
      { path: '', name: "Roles", component: () => import('pages/orga/CerclesPage.vue') },
      { path: 'role/:role?', name: "Role", component: () => import('pages/orga/CerclesPage.vue'), props: true },
      { path: 'settings', name: "Settings", component: () => import('pages/orga/SettingsPage.vue') },
    ]
  },
  {
    path: '/orga/:orga/print',
    component: () => import('src/layouts/OrgaPrintLayout.vue'),
    props: true,
    children: [
      { path: ':role_id?', name: "Print", component: () => import('pages/orga/PrintPage.vue'), props: true  },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:pathMatch(.*)',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
