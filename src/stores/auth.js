import { defineStore } from 'pinia';

import {ref, computed } from 'vue'

console.log("INIT PINIA::AUTH STORE")

export const useAuthStore = defineStore('auth', () => {
  const session = ref(null)
  const orgas = ref({a:1})

  const user = computed(() => session.value ? session.value.user : {})
  const uid = computed(() => user.value.id)
  const logged = computed(() => session.value ? true : false)

  return {user, orgas, logged, session, uid}
})
