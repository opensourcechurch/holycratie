import { defineStore } from "pinia";
import { ref, computed, watch } from "vue";
import _ from "lodash";
import { marked } from "marked";

import { SB } from "../db";
import { SupabaseAuthClient } from "@supabase/supabase-js/dist/module/lib/SupabaseAuthClient";
import { useQuasar, uid } from "quasar";

console.log("INIT PINIA::ORGA STORE");

marked.use({
  mangle: false,
  headerIds: false,
});

export const useOrgaStore = defineStore("orga", () => {
  const $q = useQuasar();

  const notify_error = (message, caption = "") =>
    $q.notify({
      message: message,
      color: "negative",
      icon: "error",
      caption: caption,
    });

  const id = ref("CLuKe4snXn85ovZMYxRQ");

  const _roles = ref([]);
  const _notes = ref([]);
  const actions = ref([]);
  const projects = ref([]);
  const documents = ref([]);
  // s5YTBMlDsHWwXZC5gc0G

  const orgas = ref([]); // Toutes les orgas
  const orga_id = ref(); // id de l'orga en cours
  const orga = computed(
    () => orgas.value.find((o) => o.id == orga_id.value) || {}
  );
  const loading = ref(false);

  /*
    SUPABASE
  */

  const fetchOrgas = async () => {
    console.log("Fetching Orgas:");
    loading.value = true;
    const { data } = await SB.from("holycratie_orgas").select();
    orgas.value = data;
    loading.value = false;
    console.log("Orgas:", data);
  };
  fetchOrgas();
  var orgas_subscription = SB.channel("any")
    .on(
      "postgres_changes",
      { event: "*", schema: "public", table: "holycratie_orgas" },
      (payload) => {
        console.log("Change received on table holycratie_orgas", payload);
        fetchOrgas();
      }
    )
    .subscribe();

  // Fetch roles from supabase
  const fetchRoles = async () => {
    // Emtpy roles if no orga_id
    if (!orga_id.value) {
      console.log("Cleaning Roles");
      _roles.value = [];
      return;
    }
    console.log("Fetching Roles:");
    const { data } = await SB.from("holycratie_roles")
      .select(
        "id, name, domain, purpose, accountabilities, parent, actors, data"
      )
      .eq("orga", orga_id.value);
    _roles.value = data;
    console.log("Roles:", data.length);
  };

  // Fetch roles from supabase
  const fetchNotes = async () => {
    if (!orga_id.value) {
      console.log("Cleaning Notes");
      _notes.value = [];
      return;
    }
    console.log("Fetching Notes:");
    const { data } = await SB.from("holycratie_notes")
      .select("id, role, title, content, data")
      .eq("orga", orga_id.value);
    _notes.value = data;
    console.log("Notes:", data.length);
  };

  var roles_subscription;

  const onRoleChange = (payload) => {
    console.log("Change received on table holycratie_roles", payload);

    // remove orga from role
    delete payload.new.orga;

    // Insert
    if (payload.new.id && !payload.old.id) _roles.value.push(payload.new);
    // Delete
    if (!payload.new.id && payload.old.id)
      _roles.value = _roles.value.filter((r) => r.id != payload.old.id);
    // Update
    if (payload.new.id && payload.old.id)
      _roles.value = _roles.value.map((r) =>
        r.id == payload.new.id ? payload.new : r
      );

    // fetchRoles()
  };

  watch(orga_id, () => {
    loading.value = true;
    if (roles_subscription) SB.removeChannel(roles_subscription);
    _roles.value = [];
    if (orga_id.value) {
      fetchRoles();
      fetchNotes();
      console.log("LISTING", {
        event: "*",
        schema: "public",
        table: "holycratie_notes",
        filter: `orga=eq.${orga_id.value}`,
      });
      // FIXME: pas sûr que le filter marche (pour les delete en tout cas)
      roles_subscription = SB.channel("any")
        .on(
          "postgres_changes",
          {
            event: "*",
            schema: "public",
            table: "holycratie_roles",
            filter: `orga=eq.${orga_id.value}`,
          },
          onRoleChange
        )
        .on(
          "postgres_changes",
          {
            event: "*",
            schema: "public",
            table: "holycratie_notes",
            filter: `orga=eq.${orga_id.value}`,
          },
          fetchNotes
        )
        // Sait pas pourquoi ça enlève la subscription aux orgas, donc je rajoute ici
        .on(
          "postgres_changes",
          { event: "*", schema: "public", table: "holycratie_orgas" },
          fetchOrgas
        )
        .subscribe();
    }
    loading.value = false;
  });

  const settings = computed(() => orga.value.settings || {});
  const templates = computed(() => settings.value.templates || []);
  const templates_names = computed(() => templates.value.map((t) => t.name));
  const actors = computed(() => orga.value.actors || []);

  // ORGA

  /**
   * Créer une nouvelle organisation
   *
   * @params {string} payload.owner - owner de l'orga
   * @params {string} payload.name - nom de l'organisation
   */
  const newOrga = async (payload) => {
    var item = {
      owner: payload.owner,
      name: payload.name,
      settings: { private: true },
    };
    console.log(item);
    const r = await SB.from("holycratie_orgas").insert(item).select();
    if (r.error) console.log("Erreur newOrga:", r.error);
    else return r.data[0];
  };

  const deleteOrga = async () => {
    await SB.from("holycratie_orgas").delete().eq("id", orga_id.value);
  };

  // ROLES

  /**
   * Créer un nouveau rôle, soit vide, soit en copian d'un modèle, et le retourne
   *
   * @params {string} payload.parent - id du role parent
   * @params {string} payload.name - nom du rôle si on crée un rôle vide
   * @params {string} payload.from - id du rôle qu'on copie
   */
  const newRole = async (payload) => {
    var item = {
      parent: payload.parent.id || null,
      actors: [],
      orga: orga.value.id,
    };
    if (item.parent == orga.value.id) item.parent = null;
    if (payload.name) item.name = payload.name;
    if (payload.from !== undefined) {
      var _template =
        templates.value[payload.from.id] || roles.value.by_id(payload.from.id);
      for (var k of ["name", "domain", "accountabilities", "purpose"])
        if (_template[k]) item[k] = _template[k];
    }
    const r = await SB.from("holycratie_roles").insert(item).select();
    if (r.error) console.log(r);
    else return r.data[0];
  };

  /**
   * Créer un cercle avec les roles par défaut des templates
   *
   * @params {string} payload.parent - id du role parent
   * @params {string} payload.name - nom du rôle si on crée un rôle vide
   */
  const newCircle = async (payload) => {
    // Ajouter le cercle
    var c = await newRole({
      parent: payload.parent || null,
      actors: [],
      name: payload.name || "Nouveau cercle",
    });
    for (const [k, t] of templates.value.entries()) {
      if (t.default) {
        var item = {
          parent: c.id,
          actors: [],
          name: t.name,
          from: k,
        };
        await newRole(item);
      }
    }
  };

  /**
   * Supprime un role
   *
   * @params {string} id - id du role a supprimer
   */
  const deleteRole = async (id) => {
    var d = await SB.from("holycratie_roles").delete().eq("id", id);
    return d;
  };

  // ACTORS

  const createActor = async (name) => {
    var _actor = {
      id: uid(),
      name: name,
    };
    var d = await SB.from("holycratie_orgas")
      .update({ actors: actors.value.concat(_actor) })
      .eq("id", orga_id.value);
    return _actor;
  };

  const toggleActorInRole = async (actor, role) => {
    var _actors = _.xor(
      role.actors.map((a) => a.id),
      [actor.id]
    );
    await SB.from("holycratie_roles")
      .update({ actors: _actors })
      .eq("id", role.id);
  };

  const toggleArchive = async (role) => {
    console.log(role);
    var data = role.data;
    data.archived = !data.archived;
    await SB.from("holycratie_roles").update({ data: data }).eq("id", role.id);
  };

  /*
    COMPUTED
  */

  const show_archives = ref(false);

  /**
   * Renvoie une hierarchy de tous les roles
   *
   * @params {string} id - id du role a supprimer
   */
  const roles = computed(() => {
    // Pas encore chargé
    if (!orga.value) return {};

    console.log("LOADING ORGA", orga.value);

    var _orga = {
      id: orga.value.id,
      name: orga.value.name,
      domain: orga.value.domain,
      accountabilities: orga.value.accountabilities,
      purpose: orga.value.purpose,
      data: orga.value.data || {},
      parent: null,
      orga: true,
      roles: [],
      all_notes: [],
      _all_actors: [],
      actors: [],
      _max_depth: 0,
      // functions
      by_parent(id) {
        if (id == this.id) id = null;
        return this.roles.filter((c) => c.parent === (id || null));
      },
      by_id(id) {
        return this.roles.find((c) => c.id === id) || this;
      },
      actor_by_id(id) {
        return this._all_actors.find((a) => a.id == id);
      },
      archived_roles() {
        return this.roles.filter((r) => r.data.archived);
      },
      note_by_id(id) {
        return this.all_notes.find((n) => n.id == id);
      },
    };

    // Add roles
    _roles.value.forEach((r) => _orga.roles.push(_.cloneDeep(r)));
    _orga.roles.push(_orga);

    // Add notes
    _notes.value.forEach(n => _orga.all_notes.push(_.cloneDeep(n)))

    // Add actors
    console.log("ORGA BASE ACTORS", orga.value.actors);
    if (orga.value.actors)
      orga.value.actors.forEach((a) => {
        var actor = _.cloneDeep(a);
        actor.roles = _orga.roles.filter((r) => r.actors.includes(actor.id));
        _orga._all_actors.push(actor);
      });

    function addChildren(role, root = null) {
      role.children = [];
      if (!root) root = role;
      if (!role.orga) role.roles = [];

      role.root = root;
      root
        .by_parent(role.id)
        .filter((r) => !r.orga)
        .forEach((n) => {
          // Les éléments qui suivent seront attribués à tous les roles
          // sauf le role principal

          n.parent = role;
          if (role.data.archived) n.data.archived = true;

          // Actors
          n.actors = n.actors.map((id) => root.actor_by_id(id)) || [];

          // Children
          addChildren(n, root);

          // Archives
          if (show_archives.value || !n.data.archived) role.children.push(n);
          // Roles archivés
          n.archived_roles = () => n.root.roles.filter((r) => r.data.archived && r.parent == n);


          if (!role.orga) role.roles = role.roles.concat(n.roles);
        });

      if (!role.orga) role.roles = role.roles.concat(role.children);

      // Ces éléments seront ajoutés à tous les roles
      // Y compris le role principal

      role.is_template = templates_names.value.includes(role.name);
      role.template = templates.value.find((t) => t.name == role.name);
      role.color = settings.value.colors
        ? settings.value.colors[role.name]
        : "";
      if(role.data.print) console.log(role.name, role.data)

      // Breadcrumbs
      role.breadcrumbs = [];
      const add_bread = (r) => {
        role.breadcrumbs.push(r);
        if (r.parent) add_bread(r.parent);
      };
      add_bread(role);
      role.breadcrumbs = role.breadcrumbs.reverse();
      role.path =
        role.breadcrumbs
          .slice(0, -1)
          .map((r) => r.name)
          .join(" > ") || "Racine";
      role.fullpath =
        role.breadcrumbs.map((r) => r.name).join(" > ") || "Racine";
      role.level = role.breadcrumbs.length;
      root._max_depth = Math.max(root._max_depth, role.level);

      // Est-ce que c'est un cercle ?
      role.is_circle = false;
      if (
        role.parent == null || // cercle principal
        role.children.length || // il y a des enfants
        role.actors.length > 1
      )
        // il y a plusieurs acteurs
        role.is_circle = true;
      role.is_role = !role.is_circle;

      // Acteurs dans le cercle
      console.log("ACTORS IN CIRCLE", role.actors);
      role.actors_in_circle = role.actors;
      role.roles_in_circle = [];
      if (role.is_circle) {
        var actors = role.actors;
        // On ajoute les actors des roles
        role.children
          .filter((r) => !r.is_circle)
          .forEach((r) => {
            actors = actors.concat(r.actors);
            role.roles_in_circle.push({
              role: r,
              actors: r.actors,
              type: "role",
            });
          });
        // On ajoute les roles définis dans les settings des sous-cercles
        var t = templates.value.filter((t) => t.in_parent).map((t) => t.name);
        role.children
          .filter((c) => c.is_circle)
          .forEach((circle) => {
            // On mets les roles où il y a plusieurs actors?
            if (circle.actors.length) {
              actors = actors.concat(circle.actors);
              role.roles_in_circle.push({
                role: circle,
                actors: circle.actors,
                type: "circle",
              });
            }
            circle.children
              .filter((c) => t.includes(c.name))
              .forEach((r) => {
                r.is_functional = true
                actors = actors.concat(r.actors);
                role.roles_in_circle.push({
                  role: circle,
                  actors: r.actors,
                  type: "functional",
                  subrole: r,
                });
              });
          });
        role.actors_in_circle = _.uniq(actors);
      }

      // Acteurs dans les sous-cercles
      var all = [];
      const add_actors = (role) => {
        all = all.concat(role.actors);
        role.children.forEach((r) => add_actors(r));
      };
      add_actors(role);
      role.all_actors = _.uniq(all).filter((a) => a);

      // Notes
      // On ajoute les notes (role: null => la note appartient à la racine)
      if (!role.notes)
        role.notes = _orga.all_notes.filter(n => n.role == (role.orga ? null : role.id))
      // _.cloneDeep(
      //     _notes.value.filter((n) => n.role == (role.orga ? null : role.id))
      //   );
      // On ajoute le role à chaque note
      role.notes.forEach((n) => (n.role = role));
    }

    // Add children and stuff
    addChildren(_orga);

    // On filtre les roles qui n'ont pas de parent
    // (probablement parce qu'on est entrain de supprimer une hierarchie, ça se règle tout seul)
    _orga.orphans = _orga.roles.filter((r) => !r.actors_in_circle); // si pas cette var, c'est pas passé par addChildren
    _orga.roles = _orga.roles.filter((r) => r.actors_in_circle);
    if (_orga.orphans.length)
      console.log("WARNING, il y a des orphelins:", _orga.orphans);

    // Attribuer les roles aux actors
    _orga._all_actors.forEach((a) => {
      a.roles = _orga.roles.filter((r) => r.actors_in_circle.includes(a));
    });

    return _orga;
  });

  const template = (id) => templates.value.find((t) => t.id === id);

  /*
    ACTIONS
  */

  /**
   * Retourne l'action dont l'id est donné.
   * @param {string} id id de l'action recherchée
   */

  const actionById = (id) => actions.value.find((a) => a.id === id) || {};

  /**
   * Retourne la liste des actions dont le role est passé en argument.
   * @param {string} id id du rôle auquel est attribué l'action
   */

  const actionsByRole = (id) =>
    actions.value.filter((a) => a.role === (id || ""));

  /**
   * Retourne la liste des actions pour l'acteurice dont l'id est passé en argument
   * @param {string} id id de l'acteurice dont on veut les actions
   */

  const actionsByActor = (id) => actions.value.filter((a) => a.actor === id);

  // Utils
  // FIXME: sanitize
  const formatted = (txt) => marked(txt || "");

  /*
    SETTINGS
  */

  const set_settings = async (key, val) => {
    var s = settings.value;
    _.set(s, key, val);
    await SB.from("holycratie_orgas")
      .update({ settings: s })
      .eq("id", orga_id.value);
  };

  const remove_settings = async (path) => {
    var s = settings.value;
    _.unset(s, path);
    // Don't know why, but _.unset leaves "null" values in arrays
    s.templates = s.templates.filter((t) => t);
    await SB.from("holycratie_orgas")
      .update({ settings: s })
      .eq("id", orga_id.value);
  };

  // TERMS

  const default_terms = {
    purpose: "Raison d'être",
    accountabilities: "Redevabilités",
    domain: "Domaine",
    actors: "Acteurices",
    role: "Rôle",
    circle: "Cercle",
  };

  const terms = computed(() => {
    var terms = settings.value["terms"] || {};
    return {
      purpose: terms.purpose || default_terms.purpose,
      accountabilities:
        terms.accountabilities || default_terms.accountabilities,
      domain: terms.domain || default_terms.domain,
      actors: terms.actors || default_terms.actors,
      role: terms.role || default_terms.role,
      circle: terms.circle || default_terms.circle,
    };
  });

  const term = (t) => terms.value[t];

  return {
    // helpers
    notify_error,
    // consts
    orgas,
    id,
    _roles,
    orga_id,
    orga,
    loading,
    // supabase
    newOrga,
    deleteOrga,
    newRole,
    newCircle,
    deleteRole,
    toggleActorInRole,
    createActor,
    toggleArchive,
    // computed
    roles,
    show_archives,
    // mess
    templates,
    actors,
    actions,
    projects,
    terms,
    term,
    default_terms,
    formatted,
    actionsByActor,
    settings,
    set_settings,
    remove_settings,
  };
});
